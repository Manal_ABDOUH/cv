
        var profile = document.getElementsByClassName('profile')[0];
        var formation = document.getElementsByClassName('formation')[0];
        var competences = document.getElementsByClassName('competences')[0];

        function showElement(element){
            var el = document.getElementsByClassName(element)[0];
            el.style.display = 'block';     
        }

        function hideElement(element){
            var el = document.getElementsByClassName(element)[0];
            el.style.display = 'none';     
        }

        function getElementBtn(id){
            return document.getElementById(id).getElementsByTagName("i")[0];
        }

        function profileToggle(){
            var el="profile";
            var btn = getElementBtn(el)
            if (profile.style.display !== 'none') {
                hideElement(el);
                btn.className ="fa fa-plus";
            }
            else {
                showElement(el);
                hideElement("formation");
                hideElement("competences");

                enhancedElement(profile, 30);

                btn.className ="fa fa-minus";
            }
        }

        

        function experienceToggle(){
            var el="formation";
            var btn = getElementBtn(el)
            if (experience.style.display !== 'none') {
                hideElement(el);
                btn.className ="fa fa-plus";
            }
            else {
                showElement(el);
                hideElement("profile");
                hideElement("competences");
                
                var exps = document.getElementsByClassName("pexp");
                for (var i = 0; i < exps.length; i++) {
                    enhancedElement(exps[i], 50)
                }

                btn.className ="fa fa-minus";
            }
        }

        function competenceToggle(){
            var el="competence";
            var btn = getElementBtn(el)
            if (competence.style.display !== 'none') {
                hideElement(el);
                btn.className ="fa fa-plus";
            }
            else {
                showElement(el);
                hideElement("profile");
                hideElement("formation");
                var exps = document.getElementsByClassName("column4");
                for (var i = 0; i < exps.length; i++) {
                    enhancedElement(exps[i], 50)
                }

                btn.className ="fa fa-minus";
            }
        }


        const delay = millis => new Promise((resolve, reject) => {
        setTimeout(_ => resolve(), millis)
        });

        async function enhancedElement(element, t)  {
            for(let temp = 1; temp < t; temp++){
                element.style.height = temp+"px";
                await delay(30);
            }
            element.style.height = "auto";
            
        }



